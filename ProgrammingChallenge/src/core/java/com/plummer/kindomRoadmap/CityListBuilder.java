package com.plummer.kindomRoadmap;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CityListBuilder {

	private BufferedReader bufferedReader;
	private Map<Integer, City> cityNumberToCityMap = new HashMap<Integer, City>();

	public CityListBuilder(BufferedReader bufferedReader) {
		this.bufferedReader = bufferedReader;
	}

	public Set<City> buildCityList() {
		int numberRoadsToRead = readIntFromReader();
		Set<City> cities = new HashSet<>();
		for (int i = 0; i < numberRoadsToRead - 1; i++) {
			int[] roadAsIntArray = readIntArrayFromReader();
			City city1 = getExistingOrCreateNewCityWithNumber(roadAsIntArray[0]);
			City city2 = getExistingOrCreateNewCityWithNumber(roadAsIntArray[1]);
			city1.createRoadTo(city2);
			cities.add(city1);
			cities.add(city2);
		}
		return cities;
	}
	
	private City getExistingOrCreateNewCityWithNumber(int cityNumber) {
		City city = cityNumberToCityMap.get(cityNumber);
		if (city == null) {
			city = new City(cityNumber);
			cityNumberToCityMap.put(cityNumber,city);
		}
		return city;
	}

	private int readIntFromReader() {
		return Integer.parseInt(readLineFromReader());
	}

	private int[] readIntArrayFromReader() {
		String line = readLineFromReader();
		System.out.println(line);
		String[] roadAsStringArray = line.split(" ");
		return new int[] { Integer.parseInt(roadAsStringArray[0]),
				Integer.parseInt(roadAsStringArray[1]) };
	}

	private String readLineFromReader() {
		try {
			return bufferedReader.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
