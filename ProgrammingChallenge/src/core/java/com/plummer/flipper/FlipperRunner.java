package com.plummer.flipper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FlipperRunner {

	public static void main(String[] args) {
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(
				"flipper.in"));
				BufferedWriter bufferedWriter = new BufferedWriter(
						new FileWriter("flipper.out"));) {
			TestCaseProcessor testCaseProcessor = new TestCaseProcessor(
					bufferedReader, bufferedWriter, new PileCollectionBuilder(), new FlipProcessor(), new InquiryProcessor());
			while (testCaseProcessor.hasAnotherTestCase()) {
				testCaseProcessor.processNextTestCase();
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}

	}

}
