package com.plummer.flipper;

public class FlipProcessor {

	public void performFlips(int numberOfPiles, String flipDirections,
			PileCollection pileCollection) {
		for (int i = 0; i < numberOfPiles - 1; i++) {
			if (flipDirections.charAt(i) == 'R') {
				pileCollection.rightFlip();
			} else {
				pileCollection.leftFlip();
			}
		}
	}
	
}
