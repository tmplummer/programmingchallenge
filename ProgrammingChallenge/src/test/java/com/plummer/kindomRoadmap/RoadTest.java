package com.plummer.kindomRoadmap;

import static org.junit.Assert.*;

import org.junit.Test;

public class RoadTest {
	
	@Test
	public void hashCodeBasedOnToStringHashCode() {
		Road road1 = new Road(new City(1), new City(2));
		
		assertEquals(road1.toString().hashCode(), road1.hashCode());
	}
	
	@Test
	public void roadConstructorAddsTheRoadItConstructsToTheCities() {
		City city1 = new City(1);
		City city2 = new City(2);
		
		Road road1 = new Road(city1, city2);
		
		assertTrue(city1.getRoads().contains(road1));
		assertTrue(city2.getRoads().contains(road1));
	}
	
	@Test
	public void cancelRoadPlansRemovesRoadFromCitiesItConnects() {
		City city1 = new City(1);
		City city2 = new City(2);
		Road road1 = new Road(city1, city2);
		
		road1.cancelRoadPlans();
		
		assertTrue(city1.getRoads().isEmpty());
		assertTrue(city2.getRoads().isEmpty());
	}
	
	@Test
	public void toStringReturnsCityNumbersSeparatedBySpacesWithLowestCityNumberFirst() {
		Road road1 = new Road(new City(1), new City(2));
		
		assertEquals("1 2", road1.toString());
	}
	
	@Test
	public void toStringReturnsCityNumbersSeparatedBySpacesWithLowestCityNumberFirstForDifferentNumbers() {
		Road road1 = new Road(new City(5), new City(3));
		
		assertEquals("3 5", road1.toString());
	}

}
