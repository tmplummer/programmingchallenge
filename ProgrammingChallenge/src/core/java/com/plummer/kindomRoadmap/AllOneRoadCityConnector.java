package com.plummer.kindomRoadmap;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AllOneRoadCityConnector {

	public List<Road> connectAllOneRoadCities(List<Road> plannedNewRoads,
			Set<City> citiesWithOnlyOneRoad, Set<City> allCities) {
		List<City> oneRoadCityList = new ArrayList<>(citiesWithOnlyOneRoad);
		while (oneRoadCityList.size() > 1) {
			plannedNewRoads.add(new Road(oneRoadCityList.remove(0),
					oneRoadCityList.remove(0)));
		}
		if (oneRoadCityList.size() > 0) {
			plannedNewRoads
					.add(connectTheCityToAnyOtherCityExceptTheOneItIsAlreadyConnectedTo(
							allCities, oneRoadCityList.remove(0)));
		}
		return plannedNewRoads;
	}

	private Road connectTheCityToAnyOtherCityExceptTheOneItIsAlreadyConnectedTo(
			Set<City> allCities, City lastOneRoadCity) {
		City cityToConnectTo = null;
		for (City city : allCities) {
			if (!lastOneRoadCity.getRoads().get(0).citiesItConnects()
					.contains(city)) {
				cityToConnectTo = city;
			}
		}
		return new Road(lastOneRoadCity, cityToConnectTo);
	}

}
