package com.plummer.homoOrHetero;

public class Line {
	private static final String DELIMITER = " ";
	private int number;
	private LineType lineType;


	public enum LineType {INSERT, DELETE};
	
	
	public Line(String line) {
		String[] lineParts = line.split(DELIMITER);
		if ("insert".equals(lineParts[0])) {
			lineType = LineType.INSERT;
		} else {
			lineType = LineType.DELETE;
		}
		number = Integer.parseInt(lineParts[1]);
	}


	public LineType getLineType() {
		return lineType;
	}


	public Integer getNumber() {
		return number;
	}

}
