package com.plummer.kindomRoadmap;

import java.util.ArrayList;
import java.util.List;

public class Road {

	private final ArrayList<City> citiesItConnects = new ArrayList<>();
	private boolean currentlyExists = true;
	private final String toStringValue;
	private int hashCodeValue;

	public Road(City city1, City city2) {
		citiesItConnects.add(city1);
		citiesItConnects.add(city2);
		city1.addRoad(this);
		city2.addRoad(this);
		if (city1.getCityNumber() < city2.getCityNumber()) {
			toStringValue = city1.getCityNumber() + " " + city2.getCityNumber();
		} else {
			toStringValue = city2.getCityNumber() + " " + city1.getCityNumber();
		}
		hashCodeValue = toStringValue.hashCode();
	}

	public List<City> citiesItConnects() {
		return citiesItConnects;
	}

	@Override
	public int hashCode() {
		return hashCodeValue;
	}

	public boolean currentlyExists() {
		return currentlyExists;
	}

	public void markAsPlanned() {
		currentlyExists = false;
	}

	public void cancelRoadPlans() {
		for (City city : citiesItConnects) {
			city.cancelRoadPlans(this);
		}
	}

	@Override
	public String toString() {
		return toStringValue;
	}
}
