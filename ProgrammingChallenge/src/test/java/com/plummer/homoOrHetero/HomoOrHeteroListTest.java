package com.plummer.homoOrHetero;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

import com.plummer.homoOrHetero.HomoOrHeteroList.ListType;

public class HomoOrHeteroListTest {

	@Test
	public void listIsNeitherWhenContainsNoRecords() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		assertEquals(ListType.NEITHER, list.getListType());
	}
	
	@Test
	public void listIsNeitherWhenContainsOneRecord() {
		HomoOrHeteroList list = new HomoOrHeteroList();

		list.insert(1);
		
		assertEquals(ListType.NEITHER, list.getListType());
	}
	
	@Test
	public void listIsHomogenousWhenItContainsTwoEqualNumbers() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		
		assertEquals(ListType.HOMOGENEOUS, list.getListType());
	}
	
	@Test
	public void listIsHeterogeneousWhenItContainsTwoDifferentNumbers() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(2);
		
		assertEquals(ListType.HETEROGENEOUS, list.getListType());
	}
	
	@Test
	public void listIsBothWhenItContainsTwoEqualNumbersAndTwoNonEqualNumbers() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(2);
		
		assertEquals(ListType.BOTH, list.getListType());
	}
	
	@Test
	public void listIsHomogenousWhenItContainsThreeEqualNumbers() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(1);
		
		assertEquals(ListType.HOMOGENEOUS, list.getListType());
	}
	
	@Test
	public void listIsHeterogeneousWhenItContainsThreeUniqueNumbers() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(3);
		list.insert(1);
		list.insert(2);
		
		assertEquals(ListType.HETEROGENEOUS, list.getListType());
	}
	
	@Test
	public void listIsBothWhenItContainsMultipleEqualAndUniqueNumbers() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(2);
		list.insert(3);
		list.insert(1);
		
		assertEquals(ListType.BOTH, list.getListType());
	}
	
	@Test
	public void listIsHomogeneousIfItHasOnlyOneUniqueNumberAfterRemovingANumber() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(2);
		list.delete(2);
		
		assertEquals(ListType.HOMOGENEOUS, list.getListType());
	}
	
	@Test
	public void listIsStillBothWhenItStillHasMultipleUniqueAndEqualNumbersAfterRemoving() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(2);
		list.insert(2);
		list.delete(2);
		
		assertEquals(ListType.BOTH, list.getListType());
	}
	
	@Test
	public void listIsNeitherWhenRemovingAnElementCausesTheListToOnlyHaveOneNumber() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(2);
		list.insert(2);
		list.delete(2);
		list.delete(2);
		list.delete(1);
		
		assertEquals(ListType.NEITHER, list.getListType());
	}
	
	@Test
	public void listIsNeitherWhenRemovingAnElementCausesTheListToBeEmpty() {
		HomoOrHeteroList list = new HomoOrHeteroList();
		
		list.insert(1);
		list.insert(1);
		list.insert(2);
		list.insert(2);
		list.delete(2);
		list.delete(1);
		list.delete(2);
		list.delete(1);
		
		assertEquals(ListType.NEITHER, list.getListType());
	}

}
