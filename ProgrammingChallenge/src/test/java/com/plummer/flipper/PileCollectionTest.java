package com.plummer.flipper;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertSame;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.Test;
import org.mockito.InOrder;

public class PileCollectionTest {

	@Test
	public void getRemainingPileReturnsTheOnlyPileInTheCollection() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile = mock(Pile.class);

		pileCollection.add(mockPile);

		assertEquals(mockPile, pileCollection.getRemainingPile());
	}

	@Test
	public void rightFlipFlipsTheRightPileAndPutsItOnTopOfThePileToItsImmediateLeft() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);

		pileCollection.rightFlip();

		InOrder inOrder = inOrder(mockPile1, mockPile2);
		inOrder.verify(mockPile2).flip();
		inOrder.verify(mockPile1).add(mockPile2);
	}
	
	@Test
	public void rightFlipFlipsTheRightPileAndPutsItOnTopOfThePileToItsImmediateLeftWhenThreePiles() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.rightFlip();
		
		InOrder inOrder = inOrder(mockPile1, mockPile2, mockPile3);
		inOrder.verify(mockPile3).flip();
		inOrder.verify(mockPile2).add(mockPile3);
	}
	
	@Test
	public void twoRightFlipsWithThreePilesCorrectlyFlipsPiles() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.rightFlip();
		pileCollection.rightFlip();
		
		InOrder inOrder = inOrder(mockPile1, mockPile2, mockPile3);
		inOrder.verify(mockPile3).flip();
		inOrder.verify(mockPile2).add(mockPile3);
		inOrder.verify(mockPile2).flip();
		inOrder.verify(mockPile1).add(mockPile2);
	}
	
	@Test
	public void twoRightFlipsWithThreePilesEndsWithTheCorrectRemainingPile() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.rightFlip();
		pileCollection.rightFlip();
		
		assertSame(mockPile1, pileCollection.getRemainingPile());
	}

	@Test
	public void getRemainingPileReturnsFirstPileAfterARightFlipOnATwoCardPile() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);

		pileCollection.rightFlip();

		assertSame(mockPile1, pileCollection.getRemainingPile());
	}

	@Test
	public void leftFlipFlipsTheLeftPileAndPutsItOnTopOfThePileToItsImmediateRight() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);

		pileCollection.leftFlip();

		InOrder inOrder = inOrder(mockPile1, mockPile2);
		inOrder.verify(mockPile1).flip();
		inOrder.verify(mockPile2).add(mockPile1);
	}
	
	@Test
	public void leftFlipFlipsTheLeftPileAndPutsItOnTopOfThePileToItsImmediateRightWhenThreePiles() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.leftFlip();
		
		InOrder inOrder = inOrder(mockPile1, mockPile2, mockPile3);
		inOrder.verify(mockPile1).flip();
		inOrder.verify(mockPile2).add(mockPile1);
	}
	
	@Test
	public void twoLeftFlipsWithThreePilesCorrectlyFlipsPiles() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.leftFlip();
		pileCollection.leftFlip();
		
		InOrder inOrder = inOrder(mockPile1, mockPile2, mockPile3);
		inOrder.verify(mockPile1).flip();
		inOrder.verify(mockPile2).add(mockPile1);
		inOrder.verify(mockPile2).flip();
		inOrder.verify(mockPile3).add(mockPile2);
	}
	
	@Test
	public void twoLeftFlipsWithThreePilesEndsWithTheCorrectRemainingPile() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.leftFlip();
		pileCollection.leftFlip();
		
		assertSame(mockPile3, pileCollection.getRemainingPile());
	}
	
	
	@Test
	public void oneLeftFlipOneRightFlipWithThreePilesCorrectlyFlipsPiles() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.leftFlip();
		pileCollection.rightFlip();
		
		InOrder inOrder = inOrder(mockPile1, mockPile2, mockPile3);
		inOrder.verify(mockPile1).flip();
		inOrder.verify(mockPile2).add(mockPile1);
		inOrder.verify(mockPile3).flip();
		inOrder.verify(mockPile2).add(mockPile3);
	}
	
	@Test
	public void oneLeftFlipOneRightFlipWithThreePilesEndsWithTheCorrectRemainingPile() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		Pile mockPile3 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);
		pileCollection.add(mockPile3);
		
		pileCollection.leftFlip();
		pileCollection.rightFlip();
		
		assertSame(mockPile2, pileCollection.getRemainingPile());
	}

	@Test
	public void getRemainingPileReturnsSecondPileAfterALeftFlipOnATwoCardPile() {
		PileCollection pileCollection = new PileCollection();
		Pile mockPile1 = mock(Pile.class);
		Pile mockPile2 = mock(Pile.class);
		pileCollection.add(mockPile1);
		pileCollection.add(mockPile2);

		pileCollection.leftFlip();

		assertSame(mockPile2, pileCollection.getRemainingPile());
	}

}
