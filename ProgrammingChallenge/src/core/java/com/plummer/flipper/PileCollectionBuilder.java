package com.plummer.flipper;

public class PileCollectionBuilder {

	public PileCollection buildPileCollection(int numberOfPiles, String cardOrientations) {
		PileCollection pileCollection = new PileCollection();
		for (int i = 0; i < numberOfPiles; i++) {
			pileCollection
					.add(new Pile(new Card(cardOrientations.charAt(i), i+1)));
		}
		return pileCollection;
	}
	
}
