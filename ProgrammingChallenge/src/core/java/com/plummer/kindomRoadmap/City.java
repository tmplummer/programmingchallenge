package com.plummer.kindomRoadmap;

import java.util.ArrayList;
import java.util.List;

public class City {

	private ArrayList<Road> roads = new ArrayList<>();
	private final int cityNumber;

	public City(int cityNumber) {
		this.cityNumber = cityNumber;
	}

	public List<Road> getRoads() {
		return roads;
	}

	public Road createRoadTo(City otherCity) {
		return new Road(this, otherCity);
	}

	public int getCityNumber() {
		return cityNumber;
	}

	public Road planRoadTo(City otherCity) {
		Road road = createRoadTo(otherCity);
		road.markAsPlanned();
		return road;
	}

	public void cancelRoadPlans(Road road) {
		roads.remove(road);
	}

	public void addRoad(Road road) {
		roads.add(road);
	}

}
