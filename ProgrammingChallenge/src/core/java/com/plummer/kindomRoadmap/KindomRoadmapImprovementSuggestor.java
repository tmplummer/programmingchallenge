package com.plummer.kindomRoadmap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;

public class KindomRoadmapImprovementSuggestor {
	protected static EfficientRoadPlanner efficientRoadPlanner = new EfficientRoadPlanner();
	
	
	public static void main(String[] args) {
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(
				"kingdom.in"));
				PrintWriter bufferedWriter = new PrintWriter("kingdom.out");) {
			CityListBuilder cityListBuilder = new CityListBuilder(bufferedReader);
			Set<City> cities = cityListBuilder.buildCityList();
			List<Road> roadsThatShouldBeBuilt = efficientRoadPlanner.efficientlyPlanRoadsForReliableTransportation(cities);
			bufferedWriter.println(roadsThatShouldBeBuilt.size());
			for (Road road : roadsThatShouldBeBuilt) {
				bufferedWriter.println(road.toString());
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
