package com.plummer.homoOrHetero;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class HomoOrHeteroList {
	public enum ListType {
		NEITHER("neither"), HETEROGENEOUS("hetero"), HOMOGENEOUS("homo"), BOTH(
				"both");
		private String stringVersion;

		ListType(String stringVersion) {
			this.stringVersion = stringVersion;
		}

		@Override
		public String toString() {
			return stringVersion;
		}
	}

	private List<Integer> numbers = new ArrayList<>();
	private Set<Integer> uniqueNumbers = new HashSet<Integer>();

	public ListType getListType() {
		if (numbers.size() < 2) {
			return ListType.NEITHER;
		} else if (numbers.size() == 2) {
			if (numbers.get(0).equals(numbers.get(1))) {
				return ListType.HOMOGENEOUS;
			}
			return ListType.HETEROGENEOUS;
		} else {
			if (uniqueNumbers.size() == 1) {
				return ListType.HOMOGENEOUS;
			} else if (uniqueNumbers.size() == numbers.size()) {
				return ListType.HETEROGENEOUS;
			}
			return ListType.BOTH;
		}
	}

	public void insert(Integer i) {
		numbers.add(i);
		uniqueNumbers.add(i);
	}

	public void delete(Integer i) {
		numbers.remove(i);
		if (!numbers.contains(i)) {
			uniqueNumbers.remove(i);
		}
	};

}
