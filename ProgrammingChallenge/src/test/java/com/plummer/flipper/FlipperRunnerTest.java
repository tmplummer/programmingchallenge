package com.plummer.flipper;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class FlipperRunnerTest {

	private File inputFile = new File("flipper.in");
	private File outputFile = new File("flipper.out");

	@Before
	public void setup() {
		inputFile.delete();
		outputFile.delete();
	}

	@After
	public void tearDown() {
		inputFile.delete();
		outputFile.delete();
	}
	
	@Test
	public void properlyProcessesOneTestCase() throws IOException {
		setupFileWithLines("5", "UUUDD", "RRLL", "5 1 2 3 4 5", "0");
		
		FlipperRunner.main(new String[]{});
		
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("Pile 1");
		linesInOrder.add("Card 1 is a face down 2.");
		linesInOrder.add("Card 2 is a face up 1.");
		linesInOrder.add("Card 3 is a face up 4.");
		linesInOrder.add("Card 4 is a face down 5.");
		linesInOrder.add("Card 5 is a face up 3.");
		assertFileContainsLinesInThisOrder(linesInOrder);
	}
	
	@Test
	public void properlyProcessesTwoTestCases() throws IOException {
		setupFileWithLines("5", "UUUDD", "RRLL", "5 1 2 3 4 5", "10", "UUDDUUDDUU", "LLLRRRLRL", "4 3 7 6 1", "0");

		FlipperRunner.main(new String[]{});
		
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("Pile 1");
		linesInOrder.add("Card 1 is a face down 2.");
		linesInOrder.add("Card 2 is a face up 1.");
		linesInOrder.add("Card 3 is a face up 4.");
		linesInOrder.add("Card 4 is a face down 5.");
		linesInOrder.add("Card 5 is a face up 3.");
		linesInOrder.add("Pile 2");
		linesInOrder.add("Card 3 is a face down 1.");
		linesInOrder.add("Card 7 is a face down 9.");
		linesInOrder.add("Card 6 is a face up 7.");
		linesInOrder.add("Card 1 is a face down 5.");
		assertFileContainsLinesInThisOrder(linesInOrder);
	}
	
	private void assertFileContainsLinesInThisOrder(List<String> lines)
			throws FileNotFoundException, IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				outputFile));
		for (String line : lines) {
			assertEquals(line, bufferedReader.readLine());
		}
		assertNull(bufferedReader.readLine());
		bufferedReader.close();
	}

	private void setupFileWithLines(String... lines) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
				inputFile));
		for (String line : lines) {
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}
}
