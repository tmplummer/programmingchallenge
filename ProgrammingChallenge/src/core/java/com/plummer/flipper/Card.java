package com.plummer.flipper;

public class Card {
	public enum Orientation {
		FACE_UP("face up"), FACE_DOWN("face down");
		private String toStringValue;

		private Orientation(String toStringValue) {
			this.toStringValue = toStringValue;
		}

		@Override
		public String toString() {
			return toStringValue;
		}
	}

	private Orientation orientation;
	private final int cardNumber;

	public Card(char initialOrientation, int cardNumber) {
		this.cardNumber = cardNumber;
		if ('U' == initialOrientation) {
			orientation = Orientation.FACE_UP;
		} else {
			orientation = Orientation.FACE_DOWN;
		}
	}

	@Override
	public String toString() {
		return orientation + " " + cardNumber;
	}

	public void flip() {
		if (orientation == Orientation.FACE_DOWN) {
			orientation = Orientation.FACE_UP;
		} else {
			orientation = Orientation.FACE_DOWN;
		}
	}

}
