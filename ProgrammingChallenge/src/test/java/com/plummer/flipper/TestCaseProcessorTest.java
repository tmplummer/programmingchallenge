package com.plummer.flipper;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

public class TestCaseProcessorTest {
	private BufferedReader mockBufferedReader = mock(BufferedReader.class);
	private BufferedWriter mockBufferedWriter = mock(BufferedWriter.class);
	private FlipProcessor mockFlipProcessor = mock(FlipProcessor.class);
	private PileCollectionBuilder mockPileCollectionBuilder = mock(PileCollectionBuilder.class);
	private InquiryProcessor mockInquiryProcessor = mock(InquiryProcessor.class);

	@Test
	public void hasAnotherTestCaseToProcessReturnsFalseWhenReaderReturnsZero()
			throws IOException {

		when(mockBufferedReader.readLine()).thenReturn("0");
		TestCaseProcessor processor = new TestCaseProcessor(mockBufferedReader,
				mockBufferedWriter, mockPileCollectionBuilder,
				mockFlipProcessor, mockInquiryProcessor);

		assertFalse(processor.hasAnotherTestCase());
	}

	@Test
	public void hasAnotherTestCaseToProcessReturnsTrueWhenThereIsATestCaseInTheReader()
			throws IOException {
		when(mockBufferedReader.readLine()).thenReturn("5", "UUUDD", "RRLL",
				"5 1 2 3 4 5", "0");
		TestCaseProcessor processor = new TestCaseProcessor(mockBufferedReader,
				mockBufferedWriter, mockPileCollectionBuilder, mockFlipProcessor,
				mockInquiryProcessor);

		assertTrue(processor.hasAnotherTestCase());
	}

	@Test
	public void properlyProcessesSingleTestCase() throws IOException {
		String cardOrientations = "UUUDD";
		String inquiryString = "5 1 2 3 4 5";
		String flipDirections = "RRLL";
		when(mockBufferedReader.readLine()).thenReturn("5", cardOrientations,
				flipDirections, inquiryString, "0");
		PileCollection mockPileCollection = mock(PileCollection.class);
		doReturn(mockPileCollection).when(mockPileCollectionBuilder)
				.buildPileCollection(5, cardOrientations);
		Pile mockPile = mock(Pile.class);
		doReturn(mockPile).when(mockPileCollection).getRemainingPile();
		ArrayList<String> results = new ArrayList<String>();
		results.add("result 1");
		results.add("result 2");
		doReturn(results).when(mockInquiryProcessor).processInquiries(
				inquiryString.split(" "), mockPile);
		TestCaseProcessor processor = new TestCaseProcessor(mockBufferedReader,
				mockBufferedWriter, mockPileCollectionBuilder, mockFlipProcessor,
				mockInquiryProcessor);

		processor.processNextTestCase();

		InOrder inOrder = inOrder(mockFlipProcessor, mockInquiryProcessor);
		inOrder.verify(mockFlipProcessor).performFlips(5, flipDirections,
				mockPileCollection);
		inOrder.verify(mockInquiryProcessor).processInquiries(
				inquiryString.split(" "), mockPile);
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("Pile 1");
		linesInOrder.addAll(results);
		verifyLinesWrittenToWriterInCorrectOrder(linesInOrder);
		assertFalse(processor.hasAnotherTestCase());
	}

	@Test
	public void properlyProcessesTwoTestCases() throws IOException {
		String cardOrientations1 = "UUUDD";
		String inquiryString1 = "5 1 2 3 4 5";
		String cardOrientations2 = "UUUDD";
		String inquiryString2 = "5 1 2 3 4 5";
		String flipDirections1 = "RRLL";
		String flipDirections2 = "LLLRRRLRL";
		when(mockBufferedReader.readLine()).thenReturn("5", cardOrientations1,
				flipDirections1, inquiryString1, "10", cardOrientations2,
				flipDirections2, inquiryString2, "0");
		PileCollection mockPileCollection1 = mock(PileCollection.class);
		doReturn(mockPileCollection1).when(mockPileCollectionBuilder)
				.buildPileCollection(5, cardOrientations1);
		Pile mockPile1 = mock(Pile.class);
		doReturn(mockPile1).when(mockPileCollection1).getRemainingPile();
		PileCollection mockPileCollection2 = mock(PileCollection.class);
		doReturn(mockPileCollection2).when(mockPileCollectionBuilder)
				.buildPileCollection(10, cardOrientations2);
		Pile mockPile2 = mock(Pile.class);
		doReturn(mockPile2).when(mockPileCollection2).getRemainingPile();
		ArrayList<String> results1 = new ArrayList<String>();
		results1.add("result 1");
		results1.add("result 2");
		doReturn(results1).when(mockInquiryProcessor).processInquiries(
				inquiryString1.split(" "), mockPile1);
		ArrayList<String> results2 = new ArrayList<String>();
		results2.add("result 3");
		doReturn(results2).when(mockInquiryProcessor).processInquiries(
				inquiryString2.split(" "), mockPile2);
		TestCaseProcessor processor = new TestCaseProcessor(mockBufferedReader,
				mockBufferedWriter, mockPileCollectionBuilder, mockFlipProcessor,
				mockInquiryProcessor);

		processor.processNextTestCase();
		processor.processNextTestCase();

		InOrder inOrder = inOrder(mockFlipProcessor, mockInquiryProcessor);
		inOrder.verify(mockFlipProcessor).performFlips(5, flipDirections1,
				mockPileCollection1);
		inOrder.verify(mockInquiryProcessor).processInquiries(
				inquiryString1.split(" "), mockPile1);
		inOrder.verify(mockFlipProcessor).performFlips(10, flipDirections2,
				mockPileCollection2);
		inOrder.verify(mockInquiryProcessor).processInquiries(
				inquiryString2.split(" "), mockPile2);
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("Pile 1");
		linesInOrder.addAll(results1);
		linesInOrder.add("Pile 2");
		linesInOrder.addAll(results2);
		verifyLinesWrittenToWriterInCorrectOrder(linesInOrder);
		assertFalse(processor.hasAnotherTestCase());
	}

	private void verifyLinesWrittenToWriterInCorrectOrder(
			List<String> linesInOrder)
			throws IOException {
		InOrder inOrder = Mockito.inOrder(mockBufferedWriter);
		for (String line : linesInOrder) {
			inOrder.verify(mockBufferedWriter).write(line);
			inOrder.verify(mockBufferedWriter).newLine();
		}
	}

}
