package com.plummer.kindomRoadmap;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class CityListBuilderTest {

	@Test
	public void buildsAndConnectsCitiesFromBufferedReaderWhenBufferedReaderContains1Road() throws IOException {
		BufferedReader mockBufferedReader = mock(BufferedReader.class);
		when(mockBufferedReader.readLine()).thenReturn("1","1 2");
		CityListBuilder cityListBuilder = new CityListBuilder(mockBufferedReader);
		
		Set<City> cities = cityListBuilder.buildCityList();
		
		assertNotNull(cities);
		assertEquals(2, cities.size());
		assertTrue(containsCityWithNumber(cities, 1));
		assertTrue(containsCityWithNumber(cities, 2));
		List<Road> roadsFromCity1 = getCityWithNumberFrom(1, cities).getRoads();
		assertEquals(1, roadsFromCity1.size());
		List<Road> roadsFromCity2 = getCityWithNumberFrom(2, cities).getRoads();
		assertEquals(1, roadsFromCity2.size());
		Road roadFromCity1 = roadsFromCity1.get(0);
		Road roadFromCity2 = roadsFromCity2.get(0);
		assertEquals(roadFromCity1, roadFromCity2);
		assertTrue(containsCityWithNumber(roadFromCity1.citiesItConnects(), 1));
		assertTrue(containsCityWithNumber(roadFromCity2.citiesItConnects(), 1));
		assertTrue(containsCityWithNumber(roadFromCity1.citiesItConnects(), 2));
		assertTrue(containsCityWithNumber(roadFromCity2.citiesItConnects(), 2));
	}
	
	@Test
	public void buildsAndConnectsCitiesFromBufferedReaderWhenBufferedReaderContains2Roads() throws IOException {
		BufferedReader mockBufferedReader = mock(BufferedReader.class);
		when(mockBufferedReader.readLine()).thenReturn("2","1 2", "1 3");
		CityListBuilder cityListBuilder = new CityListBuilder(mockBufferedReader);
		
		Set<City> cities = cityListBuilder.buildCityList();
		
		assertEquals(3, cities.size());
		assertTrue(containsCityWithNumber(cities, 1));
		assertTrue(containsCityWithNumber(cities, 2));
		assertTrue(containsCityWithNumber(cities, 3));
		List<Road> roadsFromCity1 = getCityWithNumberFrom(1, cities).getRoads();
		assertEquals(2, roadsFromCity1.size());
		List<Road> roadsFromCity2 = getCityWithNumberFrom(2, cities).getRoads();
		assertEquals(1, roadsFromCity2.size());
		List<Road> roadsFromCity3 = getCityWithNumberFrom(3, cities).getRoads();
		assertEquals(1, roadsFromCity3.size());
		Road roadFromCity2 = roadsFromCity2.get(0);
		Road roadFromCity3 = roadsFromCity3.get(0);
		assertTrue(containsCityWithNumber(roadFromCity2.citiesItConnects(), 1));
		assertTrue(containsCityWithNumber(roadFromCity2.citiesItConnects(), 2));
		assertTrue(containsCityWithNumber(roadFromCity3.citiesItConnects(), 1));
		assertTrue(containsCityWithNumber(roadFromCity3.citiesItConnects(), 3));
		assertTrue(roadsFromCity1.contains(roadFromCity2));
		assertTrue(roadsFromCity1.contains(roadFromCity3));
	}

	private boolean containsCityWithNumber(Collection<City> cities, int cityNumber) {
		for (City city : cities) {
			if (city.getCityNumber() == cityNumber) {
				return true;
			}
		} 
		return false;
	}
	
	private City getCityWithNumberFrom(int cityNumber, Collection<City> cities) {
		for (City city : cities) {
			if (city.getCityNumber() == cityNumber) {
				return city;
			}
		} 
		return null;
	}
	
}
