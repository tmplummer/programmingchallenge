package com.plummer.flipper;

import java.util.ArrayList;
import java.util.List;

public class PileCollection {

	private List<Pile> piles = new ArrayList<>();

	public void add(Pile pile) {
		piles.add(pile);
	}

	public Pile getRemainingPile() {
		return piles.get(0);
	}

	public void rightFlip() {
		Pile pileToFlip = piles.remove(piles.size() - 1);
		pileToFlip.flip();
		piles.get(piles.size() - 1).add(pileToFlip);
	}

	public void leftFlip() {
		piles.get(0).flip();
		piles.get(1).add(piles.remove(0));
	}

}
