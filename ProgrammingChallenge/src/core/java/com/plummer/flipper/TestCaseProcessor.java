package com.plummer.flipper;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.List;

public class TestCaseProcessor {

	private int numberOfPilesInNextTestCase;
	private int currentTestCaseNumber = 1;
	private final BufferedReader bufferedReader;
	private final BufferedWriter bufferedWriter;
	private FlipProcessor flipProcessor;
	private PileCollectionBuilder pileCollectionBuilder;
	private InquiryProcessor inquiryProcessor;

	public TestCaseProcessor(BufferedReader bufferedReader,
			BufferedWriter bufferedWriter, PileCollectionBuilder pileCollectionBuilder, FlipProcessor flipProcessor, InquiryProcessor inquiryProcessor) {
		this.pileCollectionBuilder = pileCollectionBuilder;
		this.flipProcessor = flipProcessor; 
		this.inquiryProcessor = inquiryProcessor;
		this.bufferedReader = bufferedReader;
		this.bufferedWriter = bufferedWriter;
		numberOfPilesInNextTestCase = Integer
				.parseInt(readLineFromBufferedReader());
	}

	public boolean hasAnotherTestCase() {
		return numberOfPilesInNextTestCase > 0;
	}

	public void processNextTestCase() {
		String cardOrientations = readLineFromBufferedReader();
		String flipDirections = readLineFromBufferedReader();
		String[] cardInquiryParts = readLineFromBufferedReader().split(" ");
		PileCollection pileCollection = pileCollectionBuilder.buildPileCollection(numberOfPilesInNextTestCase, cardOrientations);
		flipProcessor.performFlips(numberOfPilesInNextTestCase, flipDirections, pileCollection);
		writeLineToBufferedWriter("Pile " + currentTestCaseNumber++);
		List<String> inquiryOutputLines = inquiryProcessor.processInquiries(cardInquiryParts, pileCollection.getRemainingPile());
		for (String outputLine : inquiryOutputLines) {
			writeLineToBufferedWriter(outputLine);
		}
		numberOfPilesInNextTestCase = Integer.parseInt(readLineFromBufferedReader());
	}

	private String readLineFromBufferedReader() {
		try {
			return bufferedReader.readLine();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	private void writeLineToBufferedWriter(String line) {
		try {
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		} catch (IOException e) {
			new RuntimeException(e);
		}
	}

}
