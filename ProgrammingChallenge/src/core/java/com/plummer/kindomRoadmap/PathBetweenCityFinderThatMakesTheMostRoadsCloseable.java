package com.plummer.kindomRoadmap;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PathBetweenCityFinderThatMakesTheMostRoadsCloseable {

	public Road findPathAndContectItsEndpointsWithARoad(
			Set<City> citiesWithOnlyOneRoad, List<Road> uncloseableRoads) {
		PathDetails mostUncloseableRoadsPath = findPathThatMakesTheMostRoadsCloseable(
				uncloseableRoads, citiesWithOnlyOneRoad);
		Road bestRoad = new Road(mostUncloseableRoadsPath.startingCity,
				mostUncloseableRoadsPath.endingCity);
		return bestRoad;
	}

	private PathDetails findPathThatMakesTheMostRoadsCloseable(
			List<Road> uncloseableRoads, Set<City> citiesToStartAt) {
		PathDetails mostUncloseableRoadsPath = null;
		Set<City> traversedCities = new HashSet<>();
		for (City startingCity : citiesToStartAt) {
			traversedCities.add(startingCity);
			PathDetails pathToOtherCity = new PathDetails(startingCity);
			pathToOtherCity.endingCity = startingCity;
			PathDetails mostUncloseableRoadPathThatStartsAtCity = extendPathToCrossAsManyUncloseableRoadsAsPossible(
					pathToOtherCity, uncloseableRoads, traversedCities);
			mostUncloseableRoadsPath = pathDetailsThatCrossesTheMostUncloseableRoads(
					mostUncloseableRoadsPath,
					mostUncloseableRoadPathThatStartsAtCity);
			traversedCities.remove(startingCity);
		}
		return mostUncloseableRoadsPath;
	}

	private PathDetails extendPathToCrossAsManyUncloseableRoadsAsPossible(
			PathDetails pathThatGoesThroughOtherCity,
			List<Road> uncloseableRoads, Set<City> traversedCities) {
		PathDetails mostUncloseableRoadsPath = pathThatGoesThroughOtherCity;
		for (Road road : pathThatGoesThroughOtherCity.endingCity.getRoads()) {
			City nextCity = road.citiesItConnects().get(0);
			if (nextCity == pathThatGoesThroughOtherCity.endingCity) {
				nextCity = road.citiesItConnects().get(1);
			}
			if (traversedCities.contains(nextCity)) {
				continue;
			}
			traversedCities.add(nextCity);
			PathDetails pathToOtherCity = pathThatGoesThroughOtherCity.clone();
			if (uncloseableRoads.contains(road)) {
				pathToOtherCity.numberUncloseableRoadsTaken++;
			}
			pathToOtherCity.endingCity = nextCity;
			PathDetails extendedPath = extendPathToCrossAsManyUncloseableRoadsAsPossible(
					pathToOtherCity, uncloseableRoads, traversedCities);
			mostUncloseableRoadsPath = pathDetailsThatCrossesTheMostUncloseableRoads(
					mostUncloseableRoadsPath, extendedPath);
			traversedCities.remove(nextCity);
		}
		return mostUncloseableRoadsPath;
	}

	public PathDetails pathDetailsThatCrossesTheMostUncloseableRoads(
			PathDetails mostUncloseableRoadsPath,
			PathDetails mostUncloseableRoadPathThatStartsAtCity) {
		if (mostUncloseableRoadsPath == null
				|| mostUncloseableRoadsPath.numberUncloseableRoadsTaken < mostUncloseableRoadPathThatStartsAtCity.numberUncloseableRoadsTaken) {
			return mostUncloseableRoadPathThatStartsAtCity;
		}
		return mostUncloseableRoadsPath;
	}

	private class PathDetails {
		private final City startingCity;
		private City endingCity;
		private int numberUncloseableRoadsTaken = 0;

		public PathDetails(City startingCity) {
			this.startingCity = startingCity;
		}

		@Override
		protected PathDetails clone() {
			PathDetails newPathDetails = new PathDetails(startingCity);
			newPathDetails.endingCity = endingCity;
			newPathDetails.numberUncloseableRoadsTaken = numberUncloseableRoadsTaken;
			return newPathDetails;
		}
	}

}
