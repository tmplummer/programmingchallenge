package com.plummer.flipper;

import java.util.ArrayList;
import java.util.List;

public class InquiryProcessor {

	public List<String> processInquiries(String[] cardInquiryParts, Pile remainingPile) {
		List<String> inquiryOutput = new ArrayList<>();
		int numberOfCardInquires = Integer.parseInt(cardInquiryParts[0]);
		for (int i = 1; i <= numberOfCardInquires; i++) {
			int cardNumber = Integer.parseInt(cardInquiryParts[i]);
			Card card = remainingPile.get(cardNumber);
			inquiryOutput.add("Card " + cardNumber + " is a " + card + ".");
		}
		return inquiryOutput;
	}
	
}
