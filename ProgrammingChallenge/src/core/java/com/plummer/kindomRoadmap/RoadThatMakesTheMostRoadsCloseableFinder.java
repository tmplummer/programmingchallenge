package com.plummer.kindomRoadmap;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class RoadThatMakesTheMostRoadsCloseableFinder {

	private UncloseableRoadFinder uncloseableRoadFinder;

	public RoadThatMakesTheMostRoadsCloseableFinder(
			UncloseableRoadFinder uncloseableRoadFinder) {
		this.uncloseableRoadFinder = uncloseableRoadFinder;
	}

	public Road findUsing(List<Road> uncloseableRoads,
			Set<City> citiesWithOnlyOneRoad) {
		List<PotentialRoad> allPotentialRoads = buildListOfAllPotentialRoads(citiesWithOnlyOneRoad);
		PotentialRoad bestPotentialRoad = findPotentialRoadThatMakesTheMostRoadsCloseableAmong(
				uncloseableRoads, allPotentialRoads);
		Road bestRoad = new Road(bestPotentialRoad.city1,
				bestPotentialRoad.city2);
		return bestRoad;
	}

	private PotentialRoad findPotentialRoadThatMakesTheMostRoadsCloseableAmong(
			List<Road> uncloseableRoads, List<PotentialRoad> potentialRoads) {
		PotentialRoad bestPotentialRoad = null;
		int lowestNumberOfUncloseableRoads = uncloseableRoads.size();
		for (PotentialRoad potentialRoad : potentialRoads) {
			Road plannedRoad = potentialRoad.city1
					.planRoadTo(potentialRoad.city2);
			uncloseableRoads = uncloseableRoadFinder
					.findRoadsThatCannotBeClosedWithoutPreventingTravel();
			if (uncloseableRoads.size() < lowestNumberOfUncloseableRoads) {
				bestPotentialRoad = potentialRoad;
				lowestNumberOfUncloseableRoads = uncloseableRoads.size();
			}
			plannedRoad.cancelRoadPlans();
		}
		return bestPotentialRoad;
	}

	private List<PotentialRoad> buildListOfAllPotentialRoads(Set<City> cities) {
		List<City> cityList = new ArrayList<>(cities);
		List<PotentialRoad> potentialRoads = new ArrayList<>();
		for (int i = 0; i < cityList.size(); i++) {
			for (int j = i + 1; j < cityList.size(); j++) {
				potentialRoads.add(new PotentialRoad(cityList.get(i), cityList
						.get(j)));
			}
		}
		return potentialRoads;
	}

	private class PotentialRoad {
		private final City city1;
		private final City city2;

		public PotentialRoad(City city1, City city2) {
			this.city1 = city1;
			this.city2 = city2;
		}
	}

}
