package com.plummer.kindomRoadmap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class UncloseableRoadFinderTest {

	private UncloseableRoadFinder uncloseableRoadFinder;

	@Before
	public void setup() {
		uncloseableRoadFinder = new UncloseableRoadFinder();
	}
	
	@Test
	public void findsUncloseableRoadWhenOnlyOneCityNotInACycle() {
		City city1 = new City(1);
		City city2 = new City(2);
		City city3 = new City(3);
		City city4 = new City(4);
		city1.createRoadTo(city2);
		city1.createRoadTo(city3);
		Road road1 = city1.createRoadTo(city4);
		city2.createRoadTo(city3);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		uncloseableRoadFinder.setCities(cities);
		
		List<Road> roadsThatCannotBeClosed = uncloseableRoadFinder.findRoadsThatCannotBeClosedWithoutPreventingTravel();
		
		assertEquals(1,roadsThatCannotBeClosed.size());
		assertTrue(roadsThatCannotBeClosed.contains(road1));
	}
	
	@Test
	public void findsUncloseableRoadWhenOnlyOneCityNotInACycleDifferentCitiesInCycle() {
		City city1 = new City(1);
		City city2 = new City(2);
		City city3 = new City(3);
		City city4 = new City(4);
		city2.createRoadTo(city1);
		Road road1 = city2.createRoadTo(city3);
		city2.createRoadTo(city4);
		city1.createRoadTo(city4);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		uncloseableRoadFinder.setCities(cities);
		
		List<Road> roadsThatCannotBeClosed = uncloseableRoadFinder.findRoadsThatCannotBeClosedWithoutPreventingTravel();
		
		assertEquals(1,roadsThatCannotBeClosed.size());
		assertTrue(roadsThatCannotBeClosed.contains(road1));
	}
	
	@Test
	public void findsUncloseableRoadWhenCitiesInStarFormation() {
		City city1 = new City(1);
		City city2 = new City(2);
		City city3 = new City(3);
		City city4 = new City(4);
		Road road1 = city2.createRoadTo(city1);
		Road road2 = city2.createRoadTo(city3);
		Road road3 = city2.createRoadTo(city4);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		uncloseableRoadFinder.setCities(cities);
		
		List<Road> roadsThatCannotBeClosed = uncloseableRoadFinder.findRoadsThatCannotBeClosedWithoutPreventingTravel();
		
		assertEquals(3,roadsThatCannotBeClosed.size());
		assertTrue(roadsThatCannotBeClosed.contains(road1));
		assertTrue(roadsThatCannotBeClosed.contains(road2));
		assertTrue(roadsThatCannotBeClosed.contains(road3));
	}
	
	@Test
	public void returnsEmptyListForCompleteCycle() {
		City city1 = new City(1);
		City city2 = new City(2);
		City city3 = new City(3);
		City city4 = new City(4);
		city1.createRoadTo(city2);
		city2.createRoadTo(city3);
		city3.createRoadTo(city4);
		city4.createRoadTo(city1);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		uncloseableRoadFinder.setCities(cities);
		
		List<Road> roadsThatCannotBeClosed = uncloseableRoadFinder.findRoadsThatCannotBeClosedWithoutPreventingTravel();
		
		assertEquals(0,roadsThatCannotBeClosed.size());
	}
	
}
