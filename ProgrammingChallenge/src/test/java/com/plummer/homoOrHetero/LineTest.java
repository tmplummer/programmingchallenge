package com.plummer.homoOrHetero;

import static junit.framework.Assert.assertEquals;

import org.junit.Test;

import com.plummer.homoOrHetero.Line.LineType;

public class LineTest {

	@Test
	public void correctlyParsesInsertLine() {
		Line line = new Line("insert 1");
		
		assertEquals(LineType.INSERT, line.getLineType());
		assertEquals((Integer) 1,line.getNumber());
	}
	
	@Test
	public void correctlyParsesInsertLineForADifferentNumber() {
		Line line = new Line("insert 2");
		
		assertEquals(LineType.INSERT, line.getLineType());
		assertEquals((Integer) 2,line.getNumber());
	}
	
	@Test
	public void correctlyParsesDeleteLine() {
		Line line = new Line("delete 3");
		
		assertEquals(LineType.DELETE, line.getLineType());
		assertEquals((Integer) 3,line.getNumber());
	}
	
	@Test
	public void correctlyParsesDeleteLineForADifferentNumber() {
		Line line = new Line("delete 4");
		
		assertEquals(LineType.DELETE, line.getLineType());
		assertEquals((Integer) 4,line.getNumber());
	}
	
}
