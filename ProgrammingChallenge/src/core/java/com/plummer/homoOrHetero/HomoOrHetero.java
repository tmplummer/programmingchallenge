package com.plummer.homoOrHetero;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.plummer.homoOrHetero.Line.LineType;

public class HomoOrHetero {

	public static void main(String[] strings) {
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(
				"homo.in"));
				BufferedWriter bufferedWriter = new BufferedWriter(
						new FileWriter("homo.out"));) {
			HomoOrHeteroList numbers = new HomoOrHeteroList();
			String inputLine = bufferedReader.readLine();
			int numberOfLinesToProcess = Integer.parseInt(inputLine);
			for (int i = 0; i < numberOfLinesToProcess; i++) {
				inputLine = bufferedReader.readLine();
				Line line = new Line(inputLine);
				if (line.getLineType() == LineType.INSERT) {
					numbers.insert(line.getNumber());
				} else {
					numbers.delete(line.getNumber());
				}
				bufferedWriter.write(numbers.getListType().toString());
				bufferedWriter.newLine();
			}
			bufferedReader.close();
			bufferedWriter.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
