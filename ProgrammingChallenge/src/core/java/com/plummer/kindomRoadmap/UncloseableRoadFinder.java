package com.plummer.kindomRoadmap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class UncloseableRoadFinder {

	private Set<City> cities;

	public List<Road> findRoadsThatCannotBeClosedWithoutPreventingTravel() {
		List<Road> uncloseableRoads = new ArrayList<>();
		Set<Road> allRoads = new HashSet<>();
		for (City city : cities) {
			allRoads.addAll(city.getRoads());
		}
		for (Road road : allRoads) {
			if (!isThereAnAlternatePathThatContectsTheRoadsThisCityTouches(road)) {
				uncloseableRoads.add(road);
			}
		}
		return uncloseableRoads;
	}

	private boolean isThereAnAlternatePathThatContectsTheRoadsThisCityTouches(
			Road closedRoad) {
		City startingCity = closedRoad.citiesItConnects().get(0);
		City endingCity = closedRoad.citiesItConnects().get(1);
		Set<Road> traversedRoads = new HashSet<>();
		traversedRoads.add(closedRoad);
		return pathExistsAvoidingTraversedAndClosedRoads(startingCity,
				endingCity, traversedRoads);
	}

	private boolean pathExistsAvoidingTraversedAndClosedRoads(
			City startingCity, City endingCity, Set<Road> traversedRoads) {
		for (Road roadToTraverse : startingCity.getRoads()) {
			if (traversedRoads.contains(roadToTraverse)) {
				continue;
			}
			if (roadToTraverse.citiesItConnects().contains(endingCity)) {
				return true;
			} else {
				traversedRoads.add(roadToTraverse);
				City otherCityOnRoadToTraverse = roadToTraverse
						.citiesItConnects().get(0);
				if (otherCityOnRoadToTraverse == startingCity) {
					otherCityOnRoadToTraverse = roadToTraverse
							.citiesItConnects().get(1);
				}
				boolean pathFound = pathExistsAvoidingTraversedAndClosedRoads(
						otherCityOnRoadToTraverse, endingCity, traversedRoads);
				if (pathFound) {
					return true;
				}
			}
		}
		return false;
	}

	public void setCities(Set<City> cities) {
		this.cities = cities;
	}
}
