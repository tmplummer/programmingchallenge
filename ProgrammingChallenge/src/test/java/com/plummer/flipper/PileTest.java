package com.plummer.flipper;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import org.junit.Test;

public class PileTest {

	@Test
	public void getReturnsCardAtIndex() {
		Card initialCard = mock(Card.class);

		Pile pile = new Pile(initialCard);

		assertSame(initialCard, pile.get(1));
	}

	@Test
	public void flippingThePileFlipsTheCardInThePile() {
		Card initialCard = mock(Card.class);

		Pile pile = new Pile(initialCard);
		pile.flip();

		verify(initialCard).flip();
	}

	@Test
	public void addPutsThePassedInPileOnTopOfThePile() {
		Card initialCard1 = mock(Card.class);
		Card initialCard2 = mock(Card.class);
		Pile pile1 = new Pile(initialCard1);
		Pile pile2 = new Pile(initialCard2);
		
		pile1.add(pile2);

		assertSame(initialCard2, pile1.get(1));
		assertSame(initialCard1, pile1.get(2));
	}
	
	@Test
	public void flipFlipsAllOfTheCardsInThePile() {
		Card initialCard1 = mock(Card.class);
		Card initialCard2 = mock(Card.class);
		Pile pile1 = new Pile(initialCard1);
		Pile pile2 = new Pile(initialCard2);
		pile1.add(pile2);
		
		pile1.flip();
		
		verify(initialCard1).flip();
		verify(initialCard2).flip();
	}
	
	@Test
	public void flipReversesTheOrderOfTheCardsInThePile() {
		Card initialCard1 = mock(Card.class);
		Card initialCard2 = mock(Card.class);
		Pile pile1 = new Pile(initialCard1);
		Pile pile2 = new Pile(initialCard2);
		pile1.add(pile2);
		
		pile1.flip();
		
		assertSame(initialCard1, pile1.get(1));
		assertSame(initialCard2, pile1.get(2));
	}
	
	@Test
	public void flipFlipsAllOfTheCardsInThePileWhenFourCardsInThePile() {
		Card initialCard1 = mock(Card.class);
		Card initialCard2 = mock(Card.class);
		Card initialCard3 = mock(Card.class);
		Card initialCard4 = mock(Card.class);
		Pile pile1 = new Pile(initialCard1);
		Pile pile2 = new Pile(initialCard2);
		Pile pile3 = new Pile(initialCard3);
		Pile pile4 = new Pile(initialCard4);
		pile1.add(pile2);
		pile1.add(pile3);
		pile1.add(pile4);
		
		pile1.flip();
		
		verify(initialCard1).flip();
		verify(initialCard2).flip();
		verify(initialCard3).flip();
		verify(initialCard4).flip();
	}
	
	@Test
	public void flipReversesTheOrderOfTheCardsInThePileWhenFourCardsInThePile() {
		Card initialCard1 = mock(Card.class);
		Card initialCard2 = mock(Card.class);
		Card initialCard3 = mock(Card.class);
		Card initialCard4 = mock(Card.class);
		Pile pile1 = new Pile(initialCard1);
		Pile pile2 = new Pile(initialCard2);
		Pile pile3 = new Pile(initialCard3);
		Pile pile4 = new Pile(initialCard4);
		pile1.add(pile2);
		pile1.add(pile3);
		pile1.add(pile4);
		
		pile1.flip();
		
		assertSame(initialCard1, pile1.get(1));
		assertSame(initialCard2, pile1.get(2));
		assertSame(initialCard3, pile1.get(3));
		assertSame(initialCard4, pile1.get(4));
	}
	
	@Test
	public void correctlyAddsPilesWithMultipleCards() {
		Card initialCard1 = mock(Card.class);
		Card initialCard2 = mock(Card.class);
		Card initialCard3 = mock(Card.class);
		Card initialCard4 = mock(Card.class);
		Pile pile1 = new Pile(initialCard1);
		Pile pile2 = new Pile(initialCard2);
		Pile pile3 = new Pile(initialCard3);
		Pile pile4 = new Pile(initialCard4);
		pile1.add(pile2);
		pile1.add(pile3);
		pile4.add(pile1);
		
		assertSame(initialCard3, pile4.get(1));
		assertSame(initialCard2, pile4.get(2));
		assertSame(initialCard1, pile4.get(3));
		assertSame(initialCard4, pile4.get(4));
	}

}
