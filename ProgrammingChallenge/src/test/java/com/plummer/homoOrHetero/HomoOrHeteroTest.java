package com.plummer.homoOrHetero;

import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class HomoOrHeteroTest {

	private File inputFile = new File("homo.in");
	private File outputFile = new File("homo.out");

	@Before
	public void setup() {
		inputFile.delete();
		outputFile.delete();
	}

	@After
	public void tearDown() {
		inputFile.delete();
		outputFile.delete();
	}

	@Test
	public void correctProcessesFileWithSingleInsert() throws IOException {
		setupFileWithLines("insert 1");

		HomoOrHetero.main(new String[] {});

		assertTrue(outputFile.exists());
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("neither");
		assertFileContainsLinesInThisOrder(linesInOrder);
	}

	@Test
	public void correctProcessesFileWithOneInsertAndOneDelete()
			throws IOException {
		setupFileWithLines("insert 1", "delete 1");

		HomoOrHetero.main(new String[] {});

		assertTrue(outputFile.exists());
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("neither");
		linesInOrder.add("neither");
		assertFileContainsLinesInThisOrder(linesInOrder);
	}

	@Test
	public void correctProcessesFileWithMultipleInsertsAndDeletes()
			throws IOException {
		setupFileWithLines("insert 1", "insert 2", "insert 1", "insert 4", "delete 1", "delete 3", "delte 2", "delete 1", "insert 4", "delete 4", "delete 4");

		HomoOrHetero.main(new String[] {});

		assertTrue(outputFile.exists());
		List<String> linesInOrder = new ArrayList<>();
		linesInOrder.add("neither");
		linesInOrder.add("hetero");
		linesInOrder.add("both");
		linesInOrder.add("both");
		linesInOrder.add("hetero");
		linesInOrder.add("hetero");
		linesInOrder.add("hetero");
		linesInOrder.add("neither");
		linesInOrder.add("homo");
		linesInOrder.add("neither");
		linesInOrder.add("neither");
		assertFileContainsLinesInThisOrder(linesInOrder);
	}

	private void assertFileContainsLinesInThisOrder(List<String> lines)
			throws FileNotFoundException, IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				outputFile));
		for (String line : lines) {
			assertEquals(line, bufferedReader.readLine());
		}
		assertNull(bufferedReader.readLine());
		bufferedReader.close();
	}

	private void setupFileWithLines(String... lines) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
				inputFile));
		bufferedWriter.write(String.valueOf(lines.length));
		bufferedWriter.newLine();
		for (String line : lines) {
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}

}
