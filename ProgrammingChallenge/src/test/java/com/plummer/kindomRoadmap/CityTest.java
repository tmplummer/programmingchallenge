package com.plummer.kindomRoadmap;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import java.util.List;

import org.junit.Test;

public class CityTest {

	@Test
	public void cityStartsWithNoRoadsToIt() {
		City city = new City(1);

		List<Road> roads = city.getRoads();

		assertNotNull(roads);
		assertEquals(0, roads.size());
	}

	@Test
	public void getRoadsReturnsRoadsWhenARoadToAnotherCityCurrentlyExists() {
		City city = new City(1);
		City city2 = new City(2);
		city.createRoadTo(city2);

		List<Road> roads = city.getRoads();

		assertEquals(1, roads.size());
		Road road = roads.get(0);
		assertTrue(road.citiesItConnects().contains(city));
		assertTrue(road.citiesItConnects().contains(city));
		assertTrue(road.currentlyExists());
	}
	
	@Test
	public void getRoadsReturnsRoadsWhenARoadIsPlannedToAnotherCity() {
		City city = new City(1);
		City city2 = new City(2);
		city.planRoadTo(city2);
		
		List<Road> roads = city.getRoads();
		
		assertEquals(1, roads.size());
		Road road = roads.get(0);
		assertTrue(road.citiesItConnects().contains(city));
		assertTrue(road.citiesItConnects().contains(city));
		assertFalse(road.currentlyExists());
	}
	
	@Test
	public void planRoadToReturnsThePlannedRoad() {
		City city = new City(1);
		City city2 = new City(2);
		
		Road road = city.planRoadTo(city2);
		
		List<Road> roads = city.getRoads();
		assertEquals(1, roads.size());
		assertSame(roads.get(0), road);
	}
	
	@Test
	public void cancelRoadPlansRemovesRoadFromListOfRoads() {
		City city = new City(1);
		City city2 = new City(2);
		Road road = city.planRoadTo(city2);
		List<Road> roads = city.getRoads();
		city.cancelRoadPlans(roads.get(0));
		
		assertEquals(0, city.getRoads().size());
	}
	
	@Test
	public void addRoadAddsRoadToListOfRoads() {
		City city = new City(1);
		
		Road mockRoad = mock(Road.class);
		city.addRoad(mockRoad);
		
		List<Road> roads = city.getRoads();
		assertEquals(1, roads.size());
		assertSame(mockRoad, roads.get(0));
	}

}
