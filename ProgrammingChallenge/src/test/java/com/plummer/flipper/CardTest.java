package com.plummer.flipper;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CardTest {

	@Test
	public void faceDown2CardToStringReturnCorrectRepresentation() {
		Card card = new Card('D', 2);
		
		assertEquals("face down 2",card.toString());
	}
	
	@Test
	public void faceUp4CardToStringReturnCorrectRepresentation() {
		Card card = new Card('U', 4);
		
		assertEquals("face up 4",card.toString());
	}
	
	@Test
	public void faceUp4CardToStringReturnsCorrectRepresentationAfterBeingFlipped() {
		Card card = new Card('U', 4);
		
		card.flip();
		
		assertEquals("face down 4",card.toString());
	}
	
	
	@Test
	public void faceUp4CardToStringReturnsCorrectRepresentationAfterBeingFlippedTwice() {
		Card card = new Card('U', 4);
		
		card.flip();
		card.flip();
		
		assertEquals("face up 4",card.toString());
	}
	

	@Test
	public void faceDown2CardToStringReturnCorrectRepresentationAfterBeingFlipped() {
		Card card = new Card('D', 2);
		
		card.flip();
		
		assertEquals("face up 2",card.toString());
	}
	
}
