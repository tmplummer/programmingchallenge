package com.plummer.kindomRoadmap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class EfficientRoadPlannerTest {
	private UncloseableRoadFinder uncloseableRoadFinder = new UncloseableRoadFinder();
	private City city1 = new City(1);
	private City city2 = new City(2);
	private City city3 = new City(3);
	private City city4 = new City(4);
	private City city5 = new City(5);
	private City city6 = new City(6);
	private City city7 = new City(7);
	private City city8 = new City(8);
	private City city9 = new City(9);
	private City city10 = new City(10);
	private City city11 = new City(11);
	private City city12 = new City(12);
	private City city13 = new City(13);
	private City city14 = new City(14);
	private City city15 = new City(15);
	private City city16 = new City(16);
	
	@Test
	public void efficientlyConnectsCitiesWhenThreeCities() {
		city1.createRoadTo(city2);
		city2.createRoadTo(city3);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);

		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);

		assertNotNull(plannedNewRoads);
		assertEquals(1, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}

	@Test
	public void efficientlyConnectsCitiesWhenFourCitiesNoBranches() {
		city1.createRoadTo(city2);
		city2.createRoadTo(city3);
		city3.createRoadTo(city4);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);

		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);

		assertNotNull(plannedNewRoads);
		assertEquals(1, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}
	
	@Test
	public void efficientlyConnectsCitiesWhenFourCitiesOnlyOneCityWithOneRoad() {
		city1.createRoadTo(city2);
		city2.createRoadTo(city3);
		city3.createRoadTo(city1);
		city1.createRoadTo(city4);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		
		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);
		
		assertNotNull(plannedNewRoads);
		assertEquals(1, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}
	
	@Test
	public void efficientlyConnectsCitiesWhenFourCitiesAllBranchedAroundOneCity() {
		city1.createRoadTo(city2);
		city1.createRoadTo(city3);
		city1.createRoadTo(city4);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		
		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);
		
		assertNotNull(plannedNewRoads);
		assertEquals(2, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}
	
	@Test
	public void efficientlyConnectsCitiesWhenEightCitiesAllBranchedAroundOneCity() {
		city1.createRoadTo(city2);
		city1.createRoadTo(city3);
		city1.createRoadTo(city4);
		city1.createRoadTo(city5);
		city1.createRoadTo(city6);
		city1.createRoadTo(city7);
		city1.createRoadTo(city8);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		cities.add(city5);
		cities.add(city6);
		cities.add(city7);
		cities.add(city8);
		
		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);
		
		assertNotNull(plannedNewRoads);
		assertEquals(4, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}
	
	@Test
	public void efficientlyConnectsCitiesWhenSixCitiesHShaped() {
		city1.createRoadTo(city2);
		city2.createRoadTo(city6);
		city2.createRoadTo(city5);
		city5.createRoadTo(city3);
		city5.createRoadTo(city4);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		cities.add(city5);
		cities.add(city6);
		
		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);
		
		assertNotNull(plannedNewRoads);
		assertEquals(2, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}

	@Test
	public void efficientlyConnectsCitiesWhenSixteenCitiesHighlyBranched() {
		city1.createRoadTo(city6);
		city2.createRoadTo(city6);
		city3.createRoadTo(city5);
		city4.createRoadTo(city5);
		city6.createRoadTo(city7);
		city5.createRoadTo(city7);
		city7.createRoadTo(city8);
		city7.createRoadTo(city10);
		city10.createRoadTo(city9);
		city10.createRoadTo(city11);
		city10.createRoadTo(city16);
		city11.createRoadTo(city12);
		city11.createRoadTo(city13);
		city16.createRoadTo(city14);
		city16.createRoadTo(city15);
		Set<City> cities = new HashSet<>();
		cities.add(city1);
		cities.add(city2);
		cities.add(city3);
		cities.add(city4);
		cities.add(city5);
		cities.add(city6);
		cities.add(city7);
		cities.add(city8);
		cities.add(city9);
		cities.add(city10);
		cities.add(city11);
		cities.add(city12);
		cities.add(city13);
		cities.add(city14);
		cities.add(city15);
		cities.add(city16);

		EfficientRoadPlanner efficientRoadBuilder = new EfficientRoadPlanner();
		List<Road> plannedNewRoads = efficientRoadBuilder
				.efficientlyPlanRoadsForReliableTransportation(cities);

		assertNotNull(plannedNewRoads);
		assertEquals(5, plannedNewRoads.size());
		uncloseableRoadFinder.setCities(cities);
		assertEquals(0, uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel()
				.size());
	}

}
