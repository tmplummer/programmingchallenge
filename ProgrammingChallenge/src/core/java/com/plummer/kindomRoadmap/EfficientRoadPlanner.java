package com.plummer.kindomRoadmap;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class EfficientRoadPlanner {
	private UncloseableRoadFinder uncloseableRoadFinder = new UncloseableRoadFinder();
	private RoadThatMakesTheMostRoadsCloseableFinder roadThatMakesTheMostRoadsCloseableFinder = new RoadThatMakesTheMostRoadsCloseableFinder(
			uncloseableRoadFinder);
	private PathBetweenCityFinderThatMakesTheMostRoadsCloseable pathBetweenCityFinderThatMakesTheMostRoadsCloseable = new PathBetweenCityFinderThatMakesTheMostRoadsCloseable();
	private AllOneRoadCityConnector allOneRoadCityConnector = new AllOneRoadCityConnector();

	public List<Road> efficientlyPlanRoadsForReliableTransportation(
			Set<City> cities) {
		List<Road> plannedNewRoads = new ArrayList<>();
		Set<City> citiesWithOnlyOneRoad = findCitiesWithOnlyOneRoad(cities);
		uncloseableRoadFinder.setCities(cities);
		List<Road> uncloseableRoads = uncloseableRoadFinder
				.findRoadsThatCannotBeClosedWithoutPreventingTravel();
		while (citiesWithOnlyOneRoad.size() != uncloseableRoads.size()) {
			Road bestRoad;
			if ((float) citiesWithOnlyOneRoad.size()
					/ (float) uncloseableRoads.size() < .8) {
				bestRoad = pathBetweenCityFinderThatMakesTheMostRoadsCloseable
						.findPathAndContectItsEndpointsWithARoad(
								citiesWithOnlyOneRoad, uncloseableRoads);
			} else {
				bestRoad = roadThatMakesTheMostRoadsCloseableFinder.findUsing(
						uncloseableRoads, citiesWithOnlyOneRoad);
			}
			citiesWithOnlyOneRoad.removeAll(bestRoad.citiesItConnects());
			uncloseableRoads = uncloseableRoadFinder
					.findRoadsThatCannotBeClosedWithoutPreventingTravel();
			plannedNewRoads.add(bestRoad);
		}
		if (uncloseableRoads.size() > 0) {
			return allOneRoadCityConnector.connectAllOneRoadCities(
					plannedNewRoads, citiesWithOnlyOneRoad, cities);
		} else {
			return plannedNewRoads;
		}
	}

	private Set<City> findCitiesWithOnlyOneRoad(Set<City> cities) {
		Set<City> citiesWithOnlyOneRoad = new HashSet<>();
		for (City city : cities) {
			if (city.getRoads().size() == 1) {
				citiesWithOnlyOneRoad.add(city);
			}
		}
		return citiesWithOnlyOneRoad;
	}

}
