package com.plummer.flipper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Pile {

	private List<Card> cards = new ArrayList<>();

	public Pile(Card initialCard) {
		this.cards.add(initialCard);
	}

	public Card get(int i) {
		return cards.get(i - 1);
	}

	public void flip() {
		Collections.reverse(cards);
		for (Card card : cards) {
			card.flip();
		}
	}

	public void add(Pile pile) {
		List<Card> cardsToAdd = new ArrayList<Card>(pile.cards);
		while (cardsToAdd.size() > 0) {
			cards.add(0, cardsToAdd.remove(cardsToAdd.size() - 1));
		}
	}

}
