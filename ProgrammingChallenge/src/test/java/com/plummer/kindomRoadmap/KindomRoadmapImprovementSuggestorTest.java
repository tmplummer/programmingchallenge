package com.plummer.kindomRoadmap;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;

public class KindomRoadmapImprovementSuggestorTest {
	private File inputFile = new File("kingdom.in");
	private File outputFile = new File("kingdom.out");

	@Before
	public void setup() {
		inputFile.delete();
		outputFile.delete();
	}

	@After
	public void tearDown() {
		inputFile.delete();
		outputFile.delete();
	}

	@Test
	public void correctlyProcessesMapAndOutputsSuggestedRoadImprovents()
			throws Exception {
		setupFileWithLines("1 2", "2 3", "3 4", "3 5");
		EfficientRoadPlanner mockEfficientRoadPlanner = mock(EfficientRoadPlanner.class);
		KindomRoadmapImprovementSuggestor.efficientRoadPlanner = mockEfficientRoadPlanner;
		List<Road> roadsToReturn = new ArrayList<>();
		roadsToReturn.add(new Road(new City(1), new City(4)));
		roadsToReturn.add(new Road(new City(4), new City(5)));
		when(
				mockEfficientRoadPlanner
						.efficientlyPlanRoadsForReliableTransportation(argThat(containsCitiesWithNumbers(
								1, 2, 3, 4, 5)))).thenReturn(roadsToReturn);

		KindomRoadmapImprovementSuggestor.main(new String[] {});

		List<String> lines = new ArrayList<>();
		lines.add("2");
		lines.add("1 4");
		lines.add("4 5");
		assertFileContainsLinesInThisOrder(lines);
	}

	@Test
	public void correctlyProcessesMapAndOutputsSuggestedRoadImproventsForDifferentCities()
			throws Exception {
		setupFileWithLines("7 8", "8 9", "1 2", "1 9", "7 9");
		EfficientRoadPlanner mockEfficientRoadPlanner = mock(EfficientRoadPlanner.class);
		KindomRoadmapImprovementSuggestor.efficientRoadPlanner = mockEfficientRoadPlanner;
		List<Road> roadsToReturn = new ArrayList<>();
		roadsToReturn.add(new Road(new City(1), new City(9)));
		roadsToReturn.add(new Road(new City(2), new City(3)));
		roadsToReturn.add(new Road(new City(4), new City(5)));
		when(
				mockEfficientRoadPlanner
						.efficientlyPlanRoadsForReliableTransportation(argThat(containsCitiesWithNumbers(
								1, 2, 7, 8, 9)))).thenReturn(roadsToReturn);

		KindomRoadmapImprovementSuggestor.main(new String[] {});

		List<String> lines = new ArrayList<>();
		lines.add("3");
		lines.add("1 9");
		lines.add("2 3");
		lines.add("4 5");
		assertFileContainsLinesInThisOrder(lines);
	}

	private ArgumentMatcher<Set<City>> containsCitiesWithNumbers(
			final int... numbers) {
		return new ArgumentMatcher<Set<City>>() {
			@SuppressWarnings("unchecked")
			@Override
			public boolean matches(Object argument) {
				if (argument instanceof Set) {
					Set<City> cities = (Set<City>) argument;
					ArrayList<Integer> actualCityNumbers = new ArrayList<>();
					for (City city : cities) {
						actualCityNumbers.add(city.getCityNumber());
					}
					ArrayList<Integer> expectedCityNumbers = new ArrayList<>();
					for (Integer number : numbers) {
						expectedCityNumbers.add(number);
					}
					return actualCityNumbers.containsAll(expectedCityNumbers)
							&& expectedCityNumbers
									.containsAll(actualCityNumbers);
				}
				return false;
			}
		};
	}

	private void assertFileContainsLinesInThisOrder(List<String> lines)
			throws FileNotFoundException, IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader(
				outputFile));
		for (String line : lines) {
			System.out.println(line);
			assertEquals(line, bufferedReader.readLine());
		}
		assertNull(bufferedReader.readLine());
		bufferedReader.close();
	}

	private void setupFileWithLines(String... lines) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(
				inputFile));
		bufferedWriter.write(String.valueOf(lines.length + 1));
		bufferedWriter.newLine();
		for (String line : lines) {
			bufferedWriter.write(line);
			bufferedWriter.newLine();
		}
		bufferedWriter.close();
	}
}
